package com.nannapat.lab5;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SelectionsortAppTest {
    @Test
    public void shouldFindMinIndexTestCases1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int pos = 0;
        int minIndex = SelectionSortApp.findMinIndex(arr, pos);
        assertEquals(4, minIndex);
    }

    @Test
    public void shouldFindMinIndexTestCases2() {
        int arr[] = { 1, 4, 3, 2, 5 };
        int pos = 1;
        int minIndex = SelectionSortApp.findMinIndex(arr, pos);
        assertEquals(3, minIndex);
    }

    @Test
    public void shouldFindMinIndexTestCases3() {
        int arr[] = { 1, 2, 3, 4, 5 };
        int pos = 2;
        int minIndex = SelectionSortApp.findMinIndex(arr, pos);
        assertEquals(2, minIndex);
    }

    @Test
    public void shouldFindMinIndexTestCases4() {
        int arr[] = { 1, 1, 1, 1, 1, 0, 1, 1 };
        int pos = 0;
        int minIndex = SelectionSortApp.findMinIndex(arr, pos);
        assertEquals(5, minIndex);
    }

    @Test
    public void shouldSwapTestCases1() {
        int arr[] = { 5, 4, 3, 2, 1, };
        int expectedArr[] = { 1, 4, 3, 2, 5 };
        int first = 0;
        int second = 4;
        SelectionSortApp.swap(arr, first, second);
        assertArrayEquals(expectedArr, arr);
    }

    @Test
    public void shouldSwapTestCases2() {
        int arr[] = { 5, 4, 3, 2, 1, };
        int expectedArr[] = { 5, 4, 3, 2, 1, };
        int first = 0;
        int second = 0;
        SelectionSortApp.swap(arr, first, second);
        assertArrayEquals(expectedArr, arr);
    }
    @Test
    public void shouldSelectionTestCases1() {
        int arr[] = { 5, 4, 3, 2, 1, };
        int sortArr[] = {1, 2, 3, 4, 5} ;
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortArr, arr);
    }

    @Test
    public void shouldSelectionTestCases2() {
        int arr[] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
        int sortArr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10} ;
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortArr, arr);
    }

    @Test
    public void shouldSelectionTestCases3() {
        int arr[] = { 6, 9, 3, 7, 10, 5, 4, 8, 2, 1 };
        int sortArr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10} ;
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortArr, arr);
    }

    @Test
    public void shouldSelectionTestCases4() {
        int arr[] = { 3, 2, 1, 5, 4, 9, 10, 6, 7, 8 };
        int sortArr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10} ;
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortArr, arr);
    }
}
