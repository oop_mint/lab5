package com.nannapat.lab5;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class BubbleSortAppTest {
    @Test
    public void sholdBubbleTestCases1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int expected[] = { 4, 3, 2, 1, 5 };
        int first = 0;
        int second = 4;
        BubbleSortApp.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void sholdBubbleTestCases2() {
        int arr[] = { 4, 3, 2, 1, 5 };
        int expected[] = { 3, 2, 1, 4, 5 };
        int first = 0;
        int second = 3;
        BubbleSortApp.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void sholdBubbleTestCases3() {
        int arr[] = { 3, 2, 1, 4, 5 };
        int expected[] = { 2, 1, 3, 4, 5 };
        int first = 0;
        int second = 2;
        BubbleSortApp.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void sholdBubbleTestCases4() {
        int arr[] = { 2, 1, 3, 4, 5 };
        int expected[] = { 1, 2, 3, 4, 5 };
        int first = 0;
        int second = 1;
        BubbleSortApp.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void sholdBubbleSortTestCases1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int expected[] = { 1, 2, 3, 4, 5 };
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void sholdBubbleSortTestCases2() {
        int arr[] = { 5, 4, 3, 2, 1, };
        int expected[] = { 1, 2, 3, 4, 5 };
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void sholdBubbleSortTestCases3() {
        int arr[] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
        int expected[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(expected, arr);
    }
}